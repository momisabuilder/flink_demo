package com.flink_demo.demo.classloader;

import java.io.InputStream;
import java.net.URL;

public class TestUrl {
	public static void main(String... args) throws Exception {
		URL url = new URL("https://dtplus-cn-shanghai.data.aliyuncs.com/dataplus_173660/porana/analysis/api/template/henfuza?workspaceCode=guozh");
		InputStream inStream = url.openStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		while ((len = inStream.read(buffer)) != -1) {
			System.out.println(new String(buffer));
		}
		inStream.close();
		// System.out.println(url.openStream().read(b));
	}
}
