package com.flink_demo.demo;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.apache.flink.api.common.functions.*;

/**
 * Hello world!
 *
 */
public class FirstApp {
	public static void main(String[] args) {
		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		DataSource<String> text = env.fromElements("To be, or not to be,--that is the question:--",
				"Whether 'tis nobler in the mind to suffer", "The slings and arrows of outrageous fortune",
				"Or to take arms against a sea of troubles,");
		text.flatMap(new LineSplitter());
	}
}
