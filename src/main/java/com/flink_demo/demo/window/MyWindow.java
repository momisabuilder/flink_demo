package com.flink_demo.demo.window;

import java.util.Collection;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.WindowAssigner;
import org.apache.flink.streaming.api.windowing.triggers.Trigger;
import org.apache.flink.streaming.api.windowing.windows.Window;
import org.apache.flink.streaming.api.windowing.assigners.WindowAssigner.WindowAssignerContext;

public class MyWindow<T> extends WindowAssigner<T, Window> {

	private static final long serialVersionUID = 1L;

	@Override
	public Collection<Window> assignWindows(T element, long timestamp, WindowAssignerContext context) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Trigger<T, Window> getDefaultTrigger(StreamExecutionEnvironment env) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TypeSerializer<Window> getWindowSerializer(ExecutionConfig executionConfig) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEventTime() {
		// TODO Auto-generated method stub
		return false;
	}

}
