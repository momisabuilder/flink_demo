package com.flink_demo.demo.eventtime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;

public class KafkaProcessTime {
	public static final String ZOOKEEPER_HOST = "192.168.2.200:2181";
	public static final String KAFKA_BROKER = "192.168.2.200:9092";
	public static final String TRANSACTION_GROUP = "kafka_demo";

	public static void main(String... args) throws Exception {
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
		env.getConfig().setAutoWatermarkInterval(500L);
		env.enableCheckpointing(500);
		env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
		env.getConfig().setGlobalJobParameters(ParameterTool.fromArgs(args));
		// configure Kafka consumer
		Properties kafkaProps = new Properties();
		kafkaProps.setProperty("zookeeper.connect", ZOOKEEPER_HOST);
		kafkaProps.setProperty("bootstrap.servers", KAFKA_BROKER);
		kafkaProps.setProperty("group.id", TRANSACTION_GROUP);

		// topicd的名字是new，schema默认使用SimpleStringSchema()即可
		DataStream<Tuple3<String, Long, String>> transaction = env
				.addSource(new FlinkKafkaConsumer010<String>("event_time", new SimpleStringSchema(), kafkaProps))
				.map(new MapFunction<String, Tuple3<String, Long, String>>() {

					/**
					 * 拆分split
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple3<String, Long, String> map(String value) throws Exception {
						String val[] = value.split(",", -1);
						return new Tuple3<String, Long, String>(val[0], Long.valueOf(val[1]), val[2]);
					}
				});
		transaction.print();
		transaction.keyBy(0)
				.window(TumblingProcessingTimeWindows.of(Time.seconds(5))).sum(1).print();
		env.execute();
	}

	public static class MyTimestampAndWatermarkAssigner
			implements AssignerWithPeriodicWatermarks<Tuple3<String, Long, String>> {
		private static final long serialVersionUID = 1L;
		private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		private long maxOutOfOrderness = 4000L; // 3.5 seconds

		private long currentMaxTimestamp = 0L;

		@Override
		public long extractTimestamp(Tuple3<String, Long, String> element, long previousElementTimestamp) {
			long current = 0;
			try {
				current = sdf.parse(element.f2).getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			currentMaxTimestamp = Math.max(currentMaxTimestamp, current);
			System.out.println(element.f2);
			return current;
		}

		@Override
		public Watermark getCurrentWatermark() {
			Watermark waterMark = new Watermark(currentMaxTimestamp - maxOutOfOrderness);
			System.out.println("waterMark:" + waterMark);
			return waterMark;
		}

	}
}
