package com.flink_demo.demo.eventtime.table;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ContinuousEventTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.Trigger;
import org.apache.flink.streaming.api.windowing.triggers.TriggerResult;
import org.apache.flink.streaming.api.windowing.windows.Window;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.java.StreamTableEnvironment;

public class KafkaProcessTimeSql_2 {
	public static final String ZOOKEEPER_HOST = "192.168.2.200:2181";
	public static final String KAFKA_BROKER = "192.168.2.200:9092";
	public static final String TRANSACTION_GROUP = "kafka_demo";

	public static void main(String... args) throws Exception {
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		// 建立tableEnv
		StreamTableEnvironment tableEnv = TableEnvironment.getTableEnvironment(env);
		env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
		env.getConfig().setAutoWatermarkInterval(10000l);
		env.enableCheckpointing(500);
		env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
		env.getConfig().setGlobalJobParameters(ParameterTool.fromArgs(args));
		// configure Kafka consumer
		Properties kafkaProps = new Properties();
		kafkaProps.setProperty("zookeeper.connect", ZOOKEEPER_HOST);
		kafkaProps.setProperty("bootstrap.servers", KAFKA_BROKER);
		kafkaProps.setProperty("group.id", TRANSACTION_GROUP);

		// topicd的名字是event_time，schema默认使用SimpleStringSchema()即可
		DataStream<Tuple3<String, Long, String>> transaction = env
				.addSource(new FlinkKafkaConsumer010<String>("event_time", new SimpleStringSchema(), kafkaProps))
				.map(new MapFunction<String, Tuple3<String, Long, String>>() {

					/**
					 * 拆分split
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple3<String, Long, String> map(String value) throws Exception {
						String val[] = value.split(",", -1);
						return new Tuple3<String, Long, String>(val[0], Long.valueOf(val[1]), val[2]);
					}
				});
		Table in = tableEnv.fromDataStream(transaction, "user, score, qdate");
		tableEnv.registerTable("usertable", in);
		Table table = tableEnv.sql("SELECT * FROM usertable where user like '0001%'");
//		Table table = tableEnv.sql("SELECT user, SUM(score) as score FROM usertable GROUP BY user");
		//
		// true的标志指示一个添加消息，false的标志表示一个收回消息
		DataStream<User> result = tableEnv.toDataStream(table, User.class);
		// tableEnv.toret
		result.print();
		env.execute();
	}

	public static class User {
		public String user;
		public long score;
		public String qdate;

		// public constructor to make it a Flink POJO
		public User() {

		}

		public User(String user, long score,String qdate) {
			this.user = user;
			this.score = score;
			this.qdate = qdate;
		}

		@Override
		public String toString() {
			return user + ":" + score + ":" + qdate;
		}
	}

}
