package com.flink_demo.demo.cep.event;

public class SubMonitorEvent extends MonitorEvent {
	private int age = 20;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + "|" + age;
	}
}
