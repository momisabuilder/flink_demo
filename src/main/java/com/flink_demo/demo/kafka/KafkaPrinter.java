package com.flink_demo.demo.kafka;

import java.util.Properties;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.Partitioner;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.transformations.StreamTransformation;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.streaming.connectors.kafka.Kafka010JsonTableSource;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;

public class KafkaPrinter {
	public static final String ZOOKEEPER_HOST = "192.168.2.200:2181";
	public static final String KAFKA_BROKER = "192.168.2.200:9092";
	public static final String TRANSACTION_GROUP = "kafka_demo";

	public static void main(String... args) throws Exception {
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
		env.enableCheckpointing(10000);
		env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);
		env.getConfig().setGlobalJobParameters(ParameterTool.fromArgs(args));

		// configure Kafka consumer
		Properties kafkaProps = new Properties();
		kafkaProps.setProperty("zookeeper.connect", ZOOKEEPER_HOST);
		kafkaProps.setProperty("bootstrap.servers", KAFKA_BROKER);
		kafkaProps.setProperty("group.id", TRANSACTION_GROUP);
//		KafkaJsonSource kjs = new Kafka010JsonTableSource(, properties, typeInfo);
		// topicd的名字是new，schema默认使用SimpleStringSchema()即可
		DataStreamSource<String> transaction = env
				.addSource(new FlinkKafkaConsumer010<String>("test-topic1", new SimpleStringSchema(), kafkaProps));

		transaction.map(new MapFunction<String, Integer>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Integer map(String val) throws Exception {
				// TODO Auto-generated method stub
				return Integer.valueOf(val);
			}
		}).print();

		env.execute();
	}
}
